# Configure the AWS Provider
provider "aws" {
  region  = "us-east-1"
}


# Create a VPC
resource "aws_vpc" "test_vpc" {
  cidr_block = "10.0.0.0/16"
  instance_tenancy = "default"
}

# Create Internet Gateway
resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.test_vpc.id

  tags = {
    Name = "myIGW"
  }
}


# Create Elastic IP
resource "aws_eip" "my_eip" {
  vpc      = true
}

# Create NAT Gateway 
resource "aws_nat_gateway" "myNAT" {
  allocation_id = aws_eip.my_eip.id
  subnet_id     = aws_subnet.public_subnet.id     # NAT Gateway always resides in Public Subnet

  tags = {
    Name = "gw NAT"
  }

  # To ensure proper ordering, it is recommended to add an explicit dependency
  # on the Internet Gateway for the VPC.
  depends_on = [aws_internet_gateway.igw]     # Just the name of the Internet Gateway, not id
}

# Create custom Public Subnet
resource "aws_subnet" "public_subnet" {
  vpc_id     = aws_vpc.test_vpc.id
  cidr_block = "10.0.1.0/24"
  map_public_ip_on_launch = "true"

  tags = {
    Name = "myPublic_subnet"
  }
}

# Create custom Private Subnet
resource "aws_subnet" "private_subnet" {
  vpc_id     = aws_vpc.test_vpc.id
  cidr_block = "10.0.2.0/24"

  tags = {
    Name = "myPrivate_subnet"
  }
}

# Create Public Route Table
resource "aws_route_table" "publicRT" {
  vpc_id = aws_vpc.test_vpc.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw.id
  }

  tags = {
    Name = "publicRT"
  }
}

# Create Private Route Table
resource "aws_route_table" "privateRT" {
  vpc_id = aws_vpc.test_vpc.id
  route {
    cidr_block = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.myNAT.id
  }

  tags = {
    Name = "privateRT"
  }
}

# Associating Public Subnet with the Route Table
resource "aws_route_table_association" "a" {
  subnet_id      = aws_subnet.public_subnet.id
  route_table_id = aws_route_table.publicRT.id
}

# Associating Private Subnet with the Route Table
resource "aws_route_table_association" "b" {
  subnet_id      = aws_subnet.private_subnet.id
  route_table_id = aws_route_table.privateRT.id
}

# Create TLS Algorithm
resource "tls_private_key" "test1" {
  algorithm   = "RSA"
}

# Create Key Pair
resource "aws_key_pair" "deployer" {
  key_name   = "deployer-key"
  public_key = tls_private_key.test1.public_key_openssh

}

# Create downloadable PEM file
resource "local_sensitive_file" "pem_our_server" {
    content  = tls_private_key.test1.private_key_pem
    filename = "our_server_keypair"
}


/* # Try this alternatively for dowloading .pem key file
output "pem_file" {
  value = tls_private_key.test1.private_key_pem
}  */


# Create Security Group and Allow inbound and outbound traffic
resource "aws_security_group" "all_traffic_allowed" {
  name        = "allow_all_traffic"
  description = "Allow inbound and outbound traffic"
  vpc_id      = aws_vpc.test_vpc.id

  ingress {
    from_port = 0
    to_port   = 0
    protocol  = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "allow_all_traffic"
  }
}

