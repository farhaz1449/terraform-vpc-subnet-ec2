terraform {
  backend "s3" {
    bucket = "ts4u-test-bucket-1"                       # Bucket name from S3
    key    = "terraform.tfstate"       # This is the path or directory terraform will create in S3 bucket
    region = "us-east-1"
  }
}
